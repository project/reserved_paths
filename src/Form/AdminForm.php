<?php

namespace Drupal\reserved_paths\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class AdminForm To administer reserved paths module.
 */
class AdminForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'reserved_paths_admin_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'reserved_paths.config',
    ];
  }

  /**
   * Get the config name for this entity & bundle.
   *
   * @return string
   *   The compiled config name.
   */
  protected function getConfigName() {
    return 'reserved_paths.config';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $this->helper($form, 'reservedpaths');

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#attributes' => ['class' => ['btn-primary']],
    ];
    return $form;
  }

  /**
   * Helper function.
   */
  public function helper(&$form, $block_id) {
    $config = $this->config($this->getConfigName());

    $field_name = 'link_paths';
    $form[$field_name] = [
      'link_paths' => [
        '#type' => 'textarea',
        '#title' => $this->t('Link paths'),
        '#description' => $this->t('The paths should be separated by new line.'),
        '#default_value' => $config->get($field_name),
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $field_name = 'link_paths';
    $this->config($this->getConfigName())->set($field_name, $form_state->getValue($field_name))->save();
  }

}
