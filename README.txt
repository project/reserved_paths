CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration

INTRODUCTION
------------

This module can prevent using any path you don't want the user to use.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/reserved_paths

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/reserved_paths


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------
The installation of this module is like other Drupal modules,
install the module and enable it.


Install as you would normally install a contributed Drupal module.
Visit https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

- To Add your reserved paths: /admin/config/reserved-paths.
